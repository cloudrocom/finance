<?php

require_once('../includes/helper.php');
require_once('../model/model.php');

if (isset($_SESSION['userid'])) {
	$balance = checkbalance($_SESSION['userid']);
	if(isset($balance['cash'])) 
		$cash = $balance['cash']; 
	else
		$cash = '0';
	echo "Balance: $" . $cash . '</br>';
	render('home');
}	
else
	render('login');
?>