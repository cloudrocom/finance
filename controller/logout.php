<?php
session_start();

//require the render function
require_once('../includes/helper.php');

//destroy the session
unset($_SESSION['userid']);
session_destroy();

//go back to login page
render('login');
?>