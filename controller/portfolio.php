<?php
session_start();
require_once('../model/model.php');
require_once('../includes/helper.php');

if (isset($_SESSION['userid']))
{
	// get the list of holdings for user
	$userid = (int)$_SESSION['userid'];
	$holdings = getshares($userid);
	$balance = checkbalance($userid);
	if(isset($balance['cash'])) 
		$cash = $balance['cash']; 
	else
		$cash = '0';
	echo "Balance: $" . $cash . '</br>';
	foreach ($holdings as $key=>$symbol) {
		$quote = getquote($symbol['symbol']);
		$holdings[$key]['last_trade'] = $quote['last_trade'];
		$holdings[$key]['cash'] = $cash;
	}
	render('portfolio', array('holdings' => $holdings));
}
else
{
	render('login');
}
?>