<?php
session_start();
require_once('../model/model.php');
require_once('../includes/helper.php');

if (isset($_POST['symbol'])){
	$symbol = htmlspecialchars($_POST['symbol']);
	$quote_data = getquote(urlencode($symbol));
}
else
	$quote_data = array("name"=>'N/A');

render('quote', array('quote_data' => $quote_data));

?>