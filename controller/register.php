<?php
session_start();
//Register controller

require_once('../model/model.php');
require_once('../includes/helper.php');

//
if (isset($_POST['name']) && isset($_POST['email']) &&  isset($_POST['password']))
{
	
	$name = $_POST['name'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$pwdhash = hash("SHA256", $password);
	
	$userid = register($name, $email, $password);
	if ($userid > 0)
	{
		$_SESSION['userid'] = $userid;
		render('home');
	}
	else
	{
		render('register');
	}
}
else //go back to form
{
	render('register');
}

?>