<?php

//Login controller

//
require_once('../model/model.php');
require_once('../includes/helper.php');


if (isset($_POST['email']) &&  isset($_POST['password']))
{
	
	$email = $_POST['email'];
	$password = $_POST['password'];
	$pwdhash = hash("SHA256", $password);
	
	$userid = login($email, $password);
	if ($userid > 0)
	{
		$_SESSION['userid'] = $userid;
		render('home');
	}
	else
	{
		echo "No user registered with this e-mail";
		render('login');
	}
}
else //go back to form
{
	render('login');
}
?>