<?php
session_start();
require_once('../model/model.php');
require_once('../includes/helper.php');

$userid = (int)$_SESSION['userid'];
$symbol = $_POST['symbol'];
$shares = $_POST['qty'];

// get account balance
$balance = checkbalance($userid);
if(isset($balance['cash'])) 
	$cash = $balance['cash']; 
else
	$cash = '0';

// get shares price
$quote = getquote($symbol);
$share_price = $quote['last_trade'];

//get shares
$holdings = getshares($userid);


	// add or update the shares in database
	sellshares($userid, $symbol, $shares);

	//deduct cash from account balance
	$total = $share_price * $_POST['qty'];
	$new_balance = $cash + $total;	
	updatebalance($userid, $new_balance);


//back to last page
$return_url = (isset($_POST["return_url"]))?urldecode($_POST["return_url"]):''; //return url
header('Location:'.$return_url);