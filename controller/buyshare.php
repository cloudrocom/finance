<?php
session_start();
require_once('../model/model.php');
require_once('../includes/helper.php');

$userid = (int)$_SESSION['userid'];
$symbol = $_POST['symbol'];
$shares = $_POST['qty'];

// get account balance
$balance = checkbalance($userid);
if(isset($balance['cash'])) 
	$cash = $balance['cash']; 
else
	$cash = '0';

// get shares price
$quote = getquote($symbol);
$share_price = $quote['last_trade'];

//check if user can buy shares
if ($cash/($share_price * $_POST['qty'])>0) {
	// add or update the shares in database
	buyshares($userid, $symbol, $shares);
	//deduct cash from account balance
	$total = $share_price * $_POST['qty'];
	$new_balance = $cash - $total;	
	updatebalance($userid, $new_balance);}

//back to last page
$return_url = (isset($_POST["return_url"]))?urldecode($_POST["return_url"]):''; //return url
header('Location:'.$return_url);