<?php
/*
 * 
 * */
 

//set database params
define('DB_HOST','localhost');
define('DB_USER','iulia');
define('DB_PASSWORD','');
define('DB_DATABASE','proiect1');

//log in
function login($email, $password)
{

	//connect to mysql
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);


	//prepare email and password
	$email = mysqli_real_escape_string($link, $email);
	$hashpw = hash("sha256", $password);

	

	//if can't connect to mysql
	if (!$link) {
		die('Could not connect: ' . mysqli_error());
	}

	// verify email and password pair
	$userid = 0;
	$query = sprintf("SELECT userid FROM users WHERE LOWER(email)='%s' AND password='%s'",strtolower($email),$hashpw);
	$resource = mysqli_query($link, $query);
	if ($resource)
	{
		$row = mysqli_fetch_row($resource);
		if (isset($row[0]))
			$userid = $row[0];
	}

	//close connection to mysql
	mysqli_close($link);
	return $userid;
}


function register($name, $email, $password)
{
	$userid = 0;
	//connect to mysql
	$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
	//if can't connect to mysql
	if (!$link) {
		die('Could not connect: ' . mysqli_error());
	}

	//prepare user name, email and password
	$name = mysqli_escape_string($link, $name);
	$email = mysqli_escape_string($link, $email);
	$hashpw = hash("sha256", $password);

	//check if email already in database
	$check_email = $link->query("SELECT email FROM users WHERE email='$email'");
	$count=$check_email->num_rows;
 
 	if ($count!==0) {
 		$msg = "<div class='alert alert-danger'>
 			<span class='glyphicon glyphicon-info-sign'></span> &nbsp; sorry email already taken !
    		</div>";
    }

    else {
    	//add user to database
		$newuser = "INSERT INTO users (name, email, password) VALUES ('$name', '$email', '$hashpw')";

		if ($link->query($newuser)) {
			/* get the userid to add the 1000$ */
			$userid = 0;
			$user = $link->query("SELECT userid FROM users WHERE LOWER(email)='$email'")->fetch_row();
			$userid = $user[0];
			//add 1000$ to user acc
			$addgift = "INSERT INTO balance (userid, cash) VALUES ('$userid', '1000')";
			$link->query($addgift);
			$msg = "<div class='alert alert-success'>
   				<span class='glyphicon glyphicon-info-sign'></span> &nbsp; successfully registered !
     			</div>";

     	}else {
     		$msg = "<div class='alert alert-danger'>
     			<span class='glyphicon glyphicon-info-sign'></span> &nbsp; error while registering !
     			</div>";
  		}
    }

	//close connection to mysql
	mysqli_close($link);
	if (isset($msg)) {	echo $msg; }
	return $userid;
}

function checkbalance($userid)
{
	// connect to database with PDO
	$dsn = 'mysql:host='.DB_HOST.';dbname='.DB_DATABASE;
	$dbh = new PDO($dsn, DB_USER, DB_PASSWORD);
	
	// get user's balance
	$stmt = $dbh->prepare("SELECT cash FROM balance WHERE userid=:userid");
	$stmt->bindValue(':userid', $userid, PDO::PARAM_STR);
	if ($stmt->execute())
	{
		$cash = $stmt->fetch();
		$dbh = null;
		return $cash;
	}
	
	// close database and return null 
	$dbh = null;
	return null;
}

function updatebalance($userid, $cash)
{
	// connect to database with PDO
	$dsn = 'mysql:host='.DB_HOST.';dbname='.DB_DATABASE;
	$dbh = new PDO($dsn, DB_USER, DB_PASSWORD);

	// update account balance to database
	// prepare statement
	$stmt = $dbh->prepare("UPDATE balance SET cash =:cash WHERE userid=:userid");
	// bind values (escape quotes, except for % and _ )
	$stmt->bindValue(':userid', $userid, PDO::PARAM_STR);
	$stmt->bindValue(':cash', $cash, PDO::PARAM_INT);
	// execute the query
	$stmt->execute();

	// close database and return null 
	$dbh = null;
	return null;
}

function getshares($userid)
{
	// connect to database with PDO
	$dsn = 'mysql:host='.DB_HOST.';dbname='.DB_DATABASE;
	$dbh = new PDO($dsn, DB_USER, DB_PASSWORD);
	
	// get user's portfolio
	$stmt = $dbh->prepare("SELECT symbol, shares FROM portfolios WHERE userid=:userid");
	$stmt->bindValue(':userid', $userid, PDO::PARAM_STR);
	if ($stmt->execute())
	{
	    $result = array();
	    while ($row = $stmt->fetch()) {
			array_push($result, $row);
	    }
		$dbh = null;
		return $result;
	}
	
	// close database and return null 
	$dbh = null;
	return null;

}

function getsharesbysymbol($userid, $symbol)
{
	// connect to database with PDO
	$dsn = 'mysql:host='.DB_HOST.';dbname='.DB_DATABASE;
	$dbh = new PDO($dsn, DB_USER, DB_PASSWORD);
	
	// get user's portfolio
	$stmt = $dbh->prepare("SELECT shares FROM portfolios WHERE userid=:userid AND symbol=:symbol");
	$stmt->bindValue(':userid', $userid, PDO::PARAM_STR);
	$stmt->bindValue(':symbol', $symbol, PDO::PARAM_STR);
	if ($stmt->execute())
	{
	    $result = array();
	    while ($row = $stmt->fetch()) {
			array_push($result, $row);
	    }
		$dbh = null;
		return $result;
	}
	
	// close database and return null 
	$dbh = null;
	return null;

}
function buyshares($userid, $symbol, $qty)
{
	// connect to database with PDO
	$dsn = 'mysql:host='.DB_HOST.';dbname='.DB_DATABASE;
	$dbh = new PDO($dsn, DB_USER, DB_PASSWORD);

	//check if the symbol is already in database and get the number of shares
	$result = getsharesbysymbol($userid, $symbol);

 	if (empty($result)) 
 		// add shares to database
		// prepare statement
		$stmt = $dbh->prepare("INSERT INTO portfolios (userid, symbol, shares) VALUES (:userid, :symbol, :shares)");
	else {
		// update shares to database
		// prepare statement
		$stmt = $dbh->prepare("UPDATE portfolios SET shares =:shares WHERE userid=:userid AND symbol=:symbol");
		$qty = $qty + $result[0]['shares'];
		}
	// bind values (escape quotes, except for % and _ )
	$stmt->bindValue(':userid', $userid, PDO::PARAM_STR);
	$stmt->bindValue(':symbol', $symbol, PDO::PARAM_STR);
	$stmt->bindValue(':shares', $qty, PDO::PARAM_INT);
	// execute the query
	$stmt->execute();//
	
	//close connection and return null
	$dbh = null;
	return null;

}

function updateshares($userid, $symbol, $qty)
{
	// connect to database with PDO
	$dsn = 'mysql:host='.DB_HOST.';dbname='.DB_DATABASE;
	$dbh = new PDO($dsn, DB_USER, DB_PASSWORD);

	// update shares to database
	// prepare statement
	$stmt = $dbh->prepare("UPDATE portfolios SET shares =:shares WHERE userid=:userid AND symbol=:symbol");
	// bind values (escape quotes, except for % and _ )
	$stmt->bindValue(':userid', $userid, PDO::PARAM_STR);
	$stmt->bindValue(':symbol', $symbol, PDO::PARAM_STR);
	$stmt->bindValue(':shares', $qty, PDO::PARAM_INT);
	// execute the query
	$stmt->execute();

	// close database and return null 
	$dbh = null;
	return null;
}

function sellshares($userid, $symbol, $qty)
{
	// connect to database with PDO
	$dsn = 'mysql:host='.DB_HOST.';dbname='.DB_DATABASE;
	$dbh = new PDO($dsn, DB_USER, DB_PASSWORD);

	//get shares number from portfolio
	$shares = getsharesbysymbol($userid, $symbol);
	if(!empty($shares))
		$result = $shares[0]['shares'] - $qty;
	if ($result != 0) {
		// update shares to database
		// prepare statement
		$stmt = $dbh->prepare("UPDATE portfolios SET shares =:shares WHERE userid=:userid AND symbol=:symbol");
		$qty = $shares[0]['shares'] - $qty;
		$stmt->bindValue(':shares', $qty, PDO::PARAM_INT);
	}
	else
		// delete shares from database
		// prepare statement
		$stmt = $dbh->prepare("DELETE FROM portfolios WHERE userid=:userid AND symbol=:symbol");

	// bind values (escape quotes, except for % and _ )
	$stmt->bindValue(':userid', $userid, PDO::PARAM_STR);
	$stmt->bindValue(':symbol', $symbol, PDO::PARAM_STR);
	
	// execute the query
	$stmt->execute();

	// close database and return null 
	$dbh = null;
	return null;
}

function getquote($symbol)
{
	$result = array();
	$url = "http://download.finance.yahoo.com/d/quotes.csv?s={$symbol}&f=sl1nghm&e=.csv";
	$handle = fopen($url, "r");
	if ($row = fgetcsv($handle))
		if (isset($row[1]))
			$result = array("symbol" => $row[0],
							"last_trade" => $row[1],
							"name" => $row[2],
							"daylow" => $row[3],
							"dayhigh" => $row[4],
							"dayrange" => $row[5]);
	fclose($handle);
	return $result;
}