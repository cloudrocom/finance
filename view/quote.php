<?php
require_once('../includes/helper.php');
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
if (!isset($quote_data["symbol"])  || $quote_data["name"]=='N/A')
{
    // No quote data
    render('header', array('title' => 'Quote'));
    print "No symbol was provided, or no quote data was found.";
}
else
{
    // Render quote for provided quote data
    render('header', array('title' => 'Quote for '.htmlspecialchars($quote_data["symbol"])));
    
?>

<table class="table">
    <tr>
        <th>Symbol</th>
        <th>Name</th>
        <th>Last Trade</th>
        <th>Day Low</th>
        <th>Day High</th>
        <th>Buy</th>
    </tr>
    <tr>
        <td><?= htmlspecialchars($quote_data["symbol"]) ?></td>
        <td><?= htmlspecialchars($quote_data["name"]) ?></td>
        <td><?= htmlspecialchars($quote_data["last_trade"]) ?></td>
        <td><?= htmlspecialchars($quote_data["daylow"]) ?></td>
        <td><?= htmlspecialchars($quote_data["dayhigh"]) ?></td>
        <td><form action="../controller/buyshare.php" method="post">
            <input id="qty" type="number" min="1" name="qty">
            <?php print '
            <input type="hidden" name="symbol" value="'.$quote_data["symbol"]. '" />
            <input type="hidden" name="return_url" value="'.$current_url.'" />'
            ?>
            <button type="submit">Buy</button>
            </form></td>
    </tr>
</table>

<?php
}

render('footer');
?>