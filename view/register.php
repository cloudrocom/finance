<?php
require_once('../includes/helper.php');
render('header', array('title' => 'Register'));
?>
<h3 class="text-center">Register!</h3>
<form method="POST" action="../controller/register.php" onsubmit="return validateForm();">
    Name <input type="text" name="name" /><br />
    E-mail address: <input type="text" name="email" /><br />
    Password: <input type="password" name="password" /><br />
	<input type="submit" value="Register" />
</form>

<script type='text/javascript'>

function validateForm()
{
	isValid = true;
	
	// check if the name was entered (min=3)
	emailField = $("input[name=name]");
	if (emailField.val().length < 3)
		isValid = false;

	// check if the email address was entered (min=6: x@x.to)
	emailField = $("input[name=email]");
	if (emailField.val().length < 6)
		isValid = false;
	
	// check if the password  was entered (min=6)
	emailField = $("input[name=password]");
	if (emailField.val().length < 6)
		isValid = false;


	return isValid;
}

// set the focus to the name field (located by id attribute)
$("input[name=name]").focus();


</script>

<?php
render('footer');
?>