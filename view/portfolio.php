<?php
require_once('../includes/helper.php');
render('header', array('title' => 'Portfolio'));
echo '<h3 class="text-center"> Portfolio</h3>';

?>

<table class="table table-striped">
    <tr>
        <th>Symbol</th>
        <th>Shares</th>
        <th>Last Trade</th>
        <th>Buy</th>
        <th>Sell</th>
    </tr>
<?php 
$shares_value = 0;
$current_url = urlencode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
foreach ($holdings as $holding)
{
    $cash = $holding["cash"];
    $price = $holding['last_trade'];
    $max_amount = $cash / $price;
    $shares_value += $holding['shares'] * $holding["last_trade"];
    print "<tr>";
    print "<td>" . htmlspecialchars($holding["symbol"]) . "</td>";
    print "<td>" . htmlspecialchars($holding["shares"]) . "</td>";
    print "<td>" . htmlspecialchars($holding["last_trade"]) . "</td>";
    print "<td>" . '<form action="../controller/buyshare.php" method="post">
                    <input id="qty" type="number" min="1" max="'. $max_amount .'" name="qty">
                    <input type="hidden" name="symbol" value="'. $holding["symbol"] .'" />
                    <input type="hidden" name="shares" value="'. $holding["shares"] .'" />
                    <input type="hidden" name="return_url" value="'.$current_url.'" />
                    <button type="submit">Buy</button>
                    </form>' . "</td>";
    print "<td>" . '<form action="../controller/sellshare.php" method="post">
                    <input id="qty" type="number" min="1" max="'. $holding["shares"] .'" name="qty">
                    <input type="hidden" name="symbol" value="'. $holding["symbol"] .'" />
                    <input type="hidden" name="shares" value="'. $holding["shares"] .'" />
                    <input type="hidden" name="return_url" value="'.$current_url.'" />
                    <button type="submit">Sell</button>
                    </form>' . "</td>";
    print "</tr>";
}

?>
</table>

<?php
$total = $cash + $shares_value;
print "<p class='text-right'> Current value of your portfolio is : $". $total ."</p>"
?>
<script type='text/javascript'>

// set the focus to the first input
$('form:first *:input[type!=hidden]:first').focus();


</script>

<?php
render('footer');
?>
