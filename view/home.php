<?php
require_once('../includes/helper.php');
render('header', array('title' => 'Proiect 1: Finance'));
?>

<ul>
	<!--Get quote name from user input-->
	<form action="../controller/quote.php" method="post">
		<li>Get quote for : 
			<input id="symbol" type="text" name="symbol">
			<button type="submit">Go</button>
		</li>
	</form>
	
	<li><a href="../controller/portfolio.php">View Portfolio</a></li>
	<li><a href="../controller/logout.php">Logout</a></li>
</ul>

<script type='text/javascript'>

// set the focus to the quote field (located by id attribute)
$("input[name=symbol").focus();


</script>

<?php
render('footer');
?>